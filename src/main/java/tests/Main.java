package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import pages.Categories;
import pages.Dashboard;
import pages.LoginPage;
import utils.BaseTest;

public class Main extends BaseTest {

    public static void main(String[] args){
        EventFiringWebDriver driver = getConfiguredDriver();

        LoginPage loginPage = new LoginPage(driver);

        loginPage.open();
        loginPage.fillEmailInput();
        loginPage.fillPasswordInput();
        loginPage.clickLoginBtn();

        Dashboard dashboard = new Dashboard(driver);

        dashboard.selectCategories();

        Categories categories = new Categories(driver);

        categories.clickNewCategoryBtn();
        categories.fillCategoryNameInput();
        categories.clickCategoryFormSubmitBtn();
        categories.fillCategoryFilterNameField();
        categories.clickSubmitFilterButtonCategory();
        categories.findCategoryInFiltredCategoryTable();

        quiteDriver(driver);
    }
}
