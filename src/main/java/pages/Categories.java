package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class Categories {
    private EventFiringWebDriver driver;
    private String categoryName = "Category_01";

    private By newCategoryBtn = By.id("page-header-desc-category-new_category");
    private By categoryNameField = By.id("name_1");
    private By categoryFormSubmitBtn = By.id("category_form_submit_btn");
    private By alertSuccess = By.className("alert-success");
    private By categoryFilterName = By.name("categoryFilter_name");
    private By submitFilterButtonCategory = By.id("submitFilterButtoncategory");
    private By tableCategory = By.id("table-category");

    public Categories(EventFiringWebDriver driver){
        this.driver = driver;
    }

    public void clickNewCategoryBtn(){
        driver.findElement(newCategoryBtn).click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(categoryNameField));
    }

    public void fillCategoryNameInput() {
        driver.findElement(categoryNameField).sendKeys(categoryName);
    }

    public void clickCategoryFormSubmitBtn(){
        driver.findElement(categoryFormSubmitBtn).click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(alertSuccess));

        System.out.println(driver.findElement(alertSuccess).getText());
    }

    public void fillCategoryFilterNameField(){
        driver.findElement(categoryFilterName).sendKeys(categoryName);
    }

    public void clickSubmitFilterButtonCategory(){
        driver.findElement(submitFilterButtonCategory).click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(tableCategory));
    }

    public void findCategoryInFiltredCategoryTable(){
        List<WebElement> tableElements = driver.findElements(By.xpath("//tr/td[3]"));

        for (WebElement elem : tableElements) {
            if(elem.getText().trim().matches(categoryName)) {
                System.out.println("Categories Table contains " + categoryName + " category");
                return;
            }
        }

        System.out.println("Categories Table doesn't contain " + categoryName + " category");
    }



}
